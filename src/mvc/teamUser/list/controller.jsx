import * as React from 'react';
import * as Parse from 'parse';
import * as Model from '../../../model';
import Form from '../form/controller';
import View from './view';
import { FilterConfig } from './filter';
import { UploadMethods } from '../../../abstract/components/upload';
import executeController from '../../../abstract/global/executeController';
import storageController from '../../../abstract/global/storageController';
export default class Controller extends React.Component {
    constructor(props) {
        super(props);
        this.load = async () => {
            const systemStore = storageController.systemStore;
            await systemStore.dispatch(systemStore.createMethod(m => {
                m.title = 'Команды проектов';
            }));
            let { query, teams, projects } = this.state;
            query = Model.TeamUser.getQuery();
            let teamsQuery = new Parse.Query(Model.Team).limit(100);
            let projectsQuery = new Parse.Query(Model.Project).limit(100);
            teams = await teamsQuery.find();
            projects = await projectsQuery.find();
            this.setState({ query, teams, projects });
        };
        this.remove = async () => {
            let { selected } = this.state;
            await Promise.all(selected.map(m => m.destroy()));
        };
        this.excel = async () => {
            try {
                const data = await Parse.Cloud.run('excel:teamUser');
                const type = 'application/vnd.ms-excel';
                const filename = 'teamuser.xlsx';
                UploadMethods.promtDownload(data, filename, type);
            }
            catch (err) {
                console.log(err);
            }
        };
        // VIEW
        this.onSelect = (selected) => this.setState({ selected });
        this.onAdd = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)();
            if (item) {
                let { filter, query } = this.state;
                query = Model.TeamUser.getQuery();
                filter = filter.apply();
                this.setState({ filter, query });
            }
        };
        this.onEdit = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)({
                id: this.state.selected[0].id
            });
            if (item) {
                let { filter, query } = this.state;
                query = Model.TeamUser.getQuery();
                filter = filter.apply();
                this.setState({ filter, query });
            }
        };
        this.onRemove = async () => {
            await executeController.tryLoad(this.remove);
            let { filter, query } = this.state;
            query = Model.TeamUser.getQuery();
            filter = filter.apply();
            this.setState({ filter, query, selected: [] });
        };
        this.onFilterConfig = (filter) => {
            filter = filter.apply();
            this.setState({ filter });
        };
        this.onExport = () => executeController.tryLoad(this.excel);
        this.state = {
            query: Model.TeamUser.getQuery(),
            selected: [],
            teams: [],
            projects: [],
            filter: new FilterConfig(() => this.state.query)
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return <View controller={this}/>;
    }
}
