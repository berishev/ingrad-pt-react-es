import * as React from 'react';
import * as Model from '../../../model';
import View from './view';
import executeController from '../../../abstract/global/executeController';
export default class Controller extends React.Component {
    constructor(props) {
        super(props);
        this.load = async () => {
            let { id } = this.props.controller.navigator.params;
            let { item } = this.state;
            if (id)
                item = await Model.TeamUser.getQuery().get(id);
            this.setState({ item });
        };
        this.save = async () => {
            let { item } = this.state;
            item = await item.save();
            this.setState({ item });
            return this.props.modal.resolve(item);
        };
        this.cancel = () => {
            return this.props.modal.reject();
        };
        this.select = async (user) => {
            const replaceAll = (original, search, replacement) => {
                return original.split(search).join(replacement);
            };
            const { item } = this.state;
            item.lastname = user.lastname;
            item.name = user.firstname;
            item.patronymic = user.patronymic;
            item.phoneCode = +user.phoneWork || null;
            if (user.personalPhone) {
                let phone = user.personalPhone;
                phone = replaceAll(phone, '+', '');
                phone = replaceAll(phone, '(', '');
                phone = replaceAll(phone, ')', '');
                phone = replaceAll(phone, '-', '');
                phone = replaceAll(phone, ' ', '');
                item.phoneMobile = phone.trim();
            }
            item.floorOrRoom = user.floorNumber && `${user.floorNumber}`;
            item.spUser = user;
            if (user.department) {
                await executeController.tryLoad(async () => {
                    let org = user.department;
                    let query = Model.Team.getQuery().equalTo('spOrg', org);
                    let teamOriginal = await query.first();
                    let attempts = 0;
                    while (!teamOriginal && attempts < 10) {
                        attempts++;
                        org = await Model.SPOrgStructure.getQuery()
                            .equalTo('spID', org.parentSPID)
                            .first();
                        if (!org || attempts > 7) {
                            item.team = null;
                            throw 'ЦУПОтдел не найден';
                        }
                        query = Model.Team.getQuery().equalTo('spOrg', org);
                        teamOriginal = await query.first();
                    }
                    item.team = teamOriginal;
                    let index = item.team.positions
                        .map(m => m.trim().toLowerCase())
                        .indexOf(user.position.trim().toLowerCase());
                    if (index == -1) {
                        item.position = null;
                        throw `Проектная роль "${user.position}" не найдена. Задайте значение сами`;
                    }
                    item.position = item.team.positions[index];
                    // console.log('has position', index != -1);
                }, { disableLoading: true });
            }
            this.onChange(item);
        };
        // VIEW
        this.onChange = (item) => this.setState({ item });
        this.onSave = () => executeController.tryLoadNotification(this.save);
        this.onCancel = () => executeController.tryLoad(this.cancel);
        this.onSelect = (user) => executeController.tryLoad(this.select, null, user);
        this.state = {
            item: new Model.TeamUser()
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return <View controller={this}/>;
    }
}
