import * as React from 'react';
import Enum from 'berish-enum';
import * as collection from 'berish-collection';
import { Root } from 'berish-react-portals/dist';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { apiController, messagesController, storageController } from '../abstract/global';
import * as Components from '../abstract/components';
import controllers from './modules';
const AppTypeEnum = Enum.createFromPrimitive('teamUser', 'project', 'team');
const AppTypeLabels = new collection.Dictionary(new collection.KeyValuePair(AppTypeEnum.teamUser, 'Команды проектов'), new collection.KeyValuePair(AppTypeEnum.project, 'Проекты'), new collection.KeyValuePair(AppTypeEnum.team, 'Отделы'));
export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.renderTab = (type, Controller) => {
            return (<Components.Tabs.Child key={type} label={AppTypeLabels.get(type)} value={type}>
        <Controller controller={null}/>
      </Components.Tabs.Child>);
        };
        this.renderRoutes = () => {
            return (<Components.Tabs.Root value={this.state.type} onChange={type => this.setState({ type })}>
        {this.renderTab(AppTypeEnum.teamUser, controllers.teamUser.list)}
        {this.renderTab(AppTypeEnum.project, controllers.project.list)}
        {this.renderTab(AppTypeEnum.team, controllers.team.list)}
      </Components.Tabs.Root>);
        };
        this.state = { loaded: false, type: AppTypeEnum.teamUser };
    }
    async componentWillMount() {
        await apiController.init();
        await storageController.localStore.load();
        this.setState({ loaded: true });
    }
    render() {
        if (!this.state.loaded)
            return messagesController.loadingGlobal(true);
        return (<>
        {this.renderRoutes()}
        <Root />
        <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick={true} rtl={false} draggable={true} pauseOnHover={true}/>
      </>);
    }
}
