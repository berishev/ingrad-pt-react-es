import * as collection from 'berish-collection';
import * as ringle from 'berish-ringle';
class ErrorController {
    constructor() {
        this.pages = new collection.Dictionary();
    }
    DRegisterError(code) {
        let self = this;
        return function (Component) {
            self.registerErrorPage(Component, code);
            return Component;
        };
    }
    registerErrorPage(component, code) {
        if (this.pages.containsKey(code))
            return;
        this.pages.add(code, component);
    }
    getErrorPage(code) {
        if (this.pages.containsKey(code))
            return this.pages.get(code);
        return null;
    }
}
export default ringle.getSingleton(ErrorController);
