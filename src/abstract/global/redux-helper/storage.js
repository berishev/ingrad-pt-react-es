import Serber from 'berish-serber';
const STORAGE_PREFIX = 'BERISH-SERBER';
export class Storage {
    constructor(storeName, storageAdapter, serberPlugins) {
        this._storeName = null;
        this._storageAdapter = null;
        this._serber = null;
        this._storeName = storeName;
        this._storageAdapter = storageAdapter;
        this._serber = Serber.scope(this._storeName).plugin(serberPlugins || Serber.defaults);
    }
    getName() {
        return STORAGE_PREFIX + '-' + this._storeName;
    }
    async save(state) {
        const newState = await this._serber.serializeAsync(state);
        const newStateStringify = JSON.stringify(newState);
        return this._storageAdapter.setItem(this.getName(), newStateStringify);
    }
    async load() {
        const stateStringify = await this._storageAdapter.getItem(this.getName());
        const newState = JSON.parse(stateStringify);
        return this._serber.deserialize(newState);
    }
    async clear() {
        await this._storageAdapter.removeItem(this.getName());
    }
}
