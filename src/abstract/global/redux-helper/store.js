export class Store {
    constructor(storeAdapterFabric, reducers, initialState) {
        this._storeAdapterFabric = null;
        this._storeAdapter = null;
        this._reducers = [];
        this._callbacks = [];
        this._stateSymbol = Symbol('state');
        this._storage = null;
        this.storeWillMount = this.storeWillMount.bind(this);
        this.storeDidMount = this.storeDidMount.bind(this);
        this.storeWillUnmount = this.storeWillUnmount.bind(this);
        this.dispatch = this.dispatch.bind(this);
        this.subscribe = this.subscribe.bind(this);
        this.get = this.get.bind(this);
        this.set = this.set.bind(this);
        this._storeAdapterFabric = storeAdapterFabric;
        this._reducers = reducers || [];
        this.state = Object.assign({}, initialState);
        this._storeAdapter = this._storeAdapterFabric((state, action) => {
            let newState = Object.assign({}, state);
            for (let reducer of this._reducers) {
                newState = reducer(newState, action);
            }
            return newState;
        }, initialState);
        this.storeWillMount();
    }
    storeWillMount() {
        // store will mount event
    }
    storeDidMount() {
        // store did mount event
    }
    storeWillUnmount() {
        // store will unmount event
    }
    setStorage(storage) {
        this._storage = storage;
        return this;
    }
    get state() {
        return this[this._stateSymbol];
    }
    set state(value) {
        this[this._stateSymbol] = value;
    }
    dispatch(action) {
        return this._storeAdapter.dispatch(action);
    }
    subscribe(config) {
        const listener = this._storeAdapter.subscribe(() => {
            this.state = this._storeAdapter.getState();
            config(this.state);
            if (this._storage) {
                this._storage.save(this.state);
            }
        });
        this._callbacks.push(config);
        if (this._callbacks.length == 1)
            this.storeDidMount();
        return () => {
            listener();
            this._callbacks = this._callbacks.filter(m => m !== config);
            if (this._callbacks.length == 0)
                this.storeWillUnmount();
        };
    }
    async load() {
        if (this._storage) {
            this.state =
                (await this._storage.load()) ||
                    this.state ||
                    this._storeAdapter.getState();
        }
    }
    async clear() {
        if (this._storage) {
            await this._storage.clear();
        }
    }
    get(key) {
        return this.state[key];
    }
    set(key, value) {
        this.state[key] = value;
    }
}
