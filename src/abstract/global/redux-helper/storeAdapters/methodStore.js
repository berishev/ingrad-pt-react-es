import guid from 'berish-guid';
import { Store } from '../store';
export class MethodStore extends Store {
    constructor() {
        super(...arguments);
        this._methodAction = guid.guid();
    }
    storeWillMount() {
        super.storeWillMount();
        this.isMethodAction = this.isMethodAction.bind(this);
        this.methodReduce = this.methodReduce.bind(this);
        this.createMethod = this.createMethod.bind(this);
        this._reducers = [this.methodReduce, ...this._reducers];
    }
    isMethodAction(action) {
        if (action.type == this._methodAction)
            return true;
        return false;
    }
    methodReduce(state, action) {
        if (this.isMethodAction(action)) {
            const self = action.reducer(this, state, action) || this;
            return Object.assign({}, self.state);
        }
        return state;
    }
    createMethod(methodReducer) {
        return { type: this._methodAction, reducer: methodReducer };
    }
}
