import Portal from 'berish-react-portals';
import ModalRoute from '../router/modalRoute';
export class NavigatorController {
    constructor() {
        this._params = null;
    }
    static init(params, controller) {
        if (!controller)
            controller = new NavigatorController();
        return controller.receive(params);
    }
    receive(params) {
        this._params = params;
        return this;
    }
    get params() {
        return this._params || {};
    }
    pushModal(component) {
        let decorate = Portal.create(ModalRoute);
        return (params) => {
            let props = {
                component,
                params
            };
            return decorate(props);
        };
    }
}
