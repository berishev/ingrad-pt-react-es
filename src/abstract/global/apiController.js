import * as Parse from 'parse';
import * as ringle from 'berish-ringle';
import configController from './configController';
class ApiController {
    init() {
        Parse.initialize(configController.api.appId, configController.api.jsKey);
        Parse.serverURL = configController.api.serverURL;
        Parse.Object['disableSingleInstance']();
    }
}
export default ringle.getSingleton(ApiController);
