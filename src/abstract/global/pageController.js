import { NavigatorController } from './navigatorController';
export class PageController {
    constructor() {
        this._navigatorController = null;
    }
    static init(params, controller) {
        if (!controller)
            controller = new PageController();
        return controller.receive(params);
    }
    receive(params) {
        this._navigatorController = NavigatorController.init(params, this._navigatorController);
        return this;
    }
    get navigator() {
        return this._navigatorController;
    }
}
