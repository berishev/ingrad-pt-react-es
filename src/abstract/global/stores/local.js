import { MethodStore } from '../redux-helper';
export class LocalStore extends MethodStore {
    get collapsed() {
        return this.get('collapsed');
    }
    set collapsed(value) {
        this.set('collapsed', value);
    }
    get currentKey() {
        return this.get('currentKey');
    }
    set currentKey(value) {
        this.set('currentKey', value);
    }
    get isAdmin() {
        return !!this.get('isAdmin');
    }
    set isAdmin(value) {
        this.set('isAdmin', value);
    }
}
