import { MethodStore } from '../redux-helper';
export class GlobalStore extends MethodStore {
    get user() {
        return this.get('user');
    }
    set user(value) {
        this.set('user', value);
    }
    get role() {
        return this.get('role');
    }
    set role(value) {
        this.set('role', value);
    }
}
