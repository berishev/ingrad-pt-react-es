import * as React from 'react';
import Divider from './divider';
export default class ButtonBar extends React.Component {
    constructor() {
        super(...arguments);
        this.renderButtons = (buttons, align) => {
            buttons = buttons || [];
            return (<div style={{
                textAlign: align,
                width: '100%',
                padding: '10px 0'
            }}>
        {buttons.map((m, i) => (<div key={i} style={{ padding: '0 7px', display: 'inline-block' }}>
            {m}
          </div>))}
      </div>);
        };
    }
    render() {
        let cols;
        if (this.props.left && this.props.right) {
            cols = (<div style={{ width: '100%' }}>
          <div style={{ flex: 1 }} key={0}>
            {this.renderButtons(this.props.left, 'left')}
          </div>
          <div style={{ flex: 1 }} key={1}>
            {this.renderButtons(this.props.right, 'right')}
          </div>
        </div>);
        }
        else if (this.props.left || this.props.right) {
            cols = this.renderButtons(this.props.left || this.props.right, 'center');
        }
        return (<div>
        {this.props.disableTopDivider ? null : <Divider isHrTag={true}/>}
        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '0 10px' }}>{cols}</div>
        {this.props.disableBottomDivider ? null : <Divider isHrTag={true}/>}
      </div>);
    }
}
