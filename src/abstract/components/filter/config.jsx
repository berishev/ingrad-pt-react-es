import { LINQ } from 'berish-linq';
import Decorators from 'berish-decorate';
import { cloneDeep } from 'lodash';
export default class AbstractConfig {
    constructor(requestCall) {
        this.collapsed = false;
        this.symbolCacheResponse = Symbol('cacheResponse');
        this.symbolAttributes = Symbol('attributes');
        this.requestCall = null;
        this.requestCall = requestCall;
        setImmediate(() => this.apply());
    }
    // Его слушает контроллер, когда рендерим
    get response() {
        return (this[this.symbolCacheResponse] || this.requestCall());
    }
    apply() {
        this.cleanCache();
        let response = this.applyMethod();
        this[this.symbolCacheResponse] = response;
        return this;
    }
    cleanCache() {
        this[this.symbolCacheResponse] = undefined;
        this.symbolCacheResponse = Symbol('response');
        return this;
    }
    cleanAttributes() {
        this[this.symbolAttributes] = undefined;
        this.symbolAttributes = Symbol('attributes');
        return this;
    }
    // Зависит от реализации в детях
    applyMethod() {
        return this.clone(this.requestCall());
    }
    get filters() {
        return [];
    }
    get attributes() {
        if (!this[this.symbolAttributes])
            this[this.symbolAttributes] = {};
        return this[this.symbolAttributes];
    }
    get(key) {
        return this.attributes[key];
    }
    set(key, value) {
        this.attributes[key] = value;
    }
    // Helpers
    clone(data) {
        return AbstractConfig.clone(data);
    }
    static clone(data) {
        return cloneDeep(data);
    }
    getLinq(obj) {
        if (obj instanceof LINQ)
            return obj;
        if (Array.isArray(obj))
            return LINQ.fromArray(obj);
        return this.getLinq([obj]);
    }
    static filterProperty(defaultValue) {
        return function (target, key) {
            let descriptor = Object.getOwnPropertyDescriptor(target, key) || {};
            delete target[key];
            descriptor.set = function (val) {
                let self = this;
                self.set(key, val);
            };
            descriptor.get = function () {
                let self = this;
                let value = self.get(key);
                return typeof value == 'undefined' ? defaultValue : value;
            };
            Object.defineProperty(target, key, descriptor);
        };
    }
}
Decorators.propertyDescriptor(AbstractConfig, 'collapsed', [
    AbstractConfig.filterProperty(false)
]);
