import * as React from 'react';
import { Row, Col, Button, Divider } from '../';
export default class Filter extends React.Component {
    render() {
        if (this.props.notUsedView)
            return this.props.children;
        let borderTopLeftRadius = '4px';
        let borderTopRightRadius = '4px';
        let borderBottomLeftRadius = '4px';
        let borderBottomRightRadius = '4px';
        let display = this.props.config && this.props.config.collapsed ? null : { display: 'none' };
        return (<div style={{
            padding: '20px',
            marginBottom: '10px',
            border: '1px solid #d9d9d9',
            borderTopLeftRadius,
            borderTopRightRadius,
            borderBottomLeftRadius,
            borderBottomRightRadius,
            ...(display || {})
        }}>
        <Row>
          <Divider>Фильтр</Divider>
        </Row>
        <Row>{this.props.children}</Row>
        {!this.props.disabledFooter && (<Row>
            <Col span={12}>
              <Button buttonTitle="Применить" placeholder="Применить фильтр" onClick={this.props.onSubmit}/>
            </Col>
            <Col span={12}>
              <Button buttonTitle="Сбросить" placeholder="Сбросить фильтр" onClick={this.props.onClear}/>
            </Col>
          </Row>)}
      </div>);
    }
}
