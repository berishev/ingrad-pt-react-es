import * as React from 'react';
import { AbstractComponent } from './abstract';
import TextField from './textfield';
export default class NumericTextField extends AbstractComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (<TextField {...this.props} regExp={this.props.max
            ? new RegExp(`^[0-9]{0,${this.props.max}}$`)
            : /^[0-9]+$/}/>);
    }
}
