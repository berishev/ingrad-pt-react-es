import * as React from 'react';
import { AbstractComponent, FormItem } from './abstract';
import TextField from './textfield';
const regExp = /^\+?[8,7]\d{0,10}$/;
class PhoneField extends AbstractComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return <TextField {...this.props} regExp={regExp} fiDisable={true}/>;
    }
}
export default FormItem(PhoneField, { fiTitleType: 'InputLabel' });
