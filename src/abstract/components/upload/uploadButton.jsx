import * as React from 'react';
import Button from '../button';
import uploadMethods from './uploadMethods';
import { FormItem } from '../abstract';
import Dropzone from 'react-dropzone';
import executeController from '../../global/executeController';
class UploadButton extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = (value) => {
            if (this.props.onUpload)
                this.props.onUpload(value);
        };
        this.uploadRAW = (file) => {
            let config = {};
            config.formats = (this.props.formats || []).map(m => m.toUpperCase());
            config.sizeMB = this.props.sizeMB == 0 ? null : config.sizeMB || 2;
            let kb = file.size && file.size / 1024;
            if (config.sizeMB != null || kb > config.sizeMB * 1024)
                throw new Error('Файл должен быть меньше 2 МБ');
            const ext = (filename) => {
                if (filename.indexOf('.') == -1)
                    return null;
                return /[.]/.exec(filename)
                    ? /[^.]+$/.exec(filename)[0].toLowerCase()
                    : undefined;
            };
            config.extension = (ext(file.name) || '').toUpperCase();
            if ((config.formats.length > 0 && config.extension == null) ||
                (config.formats.length > 0 &&
                    config.formats.indexOf(config.extension) == -1))
                throw new Error(`Файл должен быть формата: ${config.formats.join(', ')}`);
            if (this.props.onUploadMethod)
                return this.props.onUploadMethod(file, config);
            return uploadMethods.base64(file);
        };
        this.upload = async (files) => {
            let file = files[0];
            let res = await this.uploadRAW(file);
            this.onChange(res);
        };
        this.renderUploadButton = () => {
            return (<Button buttonTitle="Загрузить изображение" style={{ borderBottom: 'none' }} disableTooltip/>);
        };
        this.onUploadButton = file => {
            setImmediate(() => executeController.tryLoad(this.upload, { title: 'Загрузка файлов' }, file));
            return false;
        };
        this.state = {};
    }
    render() {
        let placeholder = this.props.placeholder || this.props.title;
        let input = (<Dropzone multiple={false} onDrop={(e, b) => this.onUploadButton(e)} style={{
            cursor: 'pointer',
            border: '1px dashed #d9d9d9',
            background: '#fafafa',
            width: '100%',
            height: '100%'
        }} className="upload-button">
        {this.renderUploadButton()}
      </Dropzone>);
        return input;
    }
}
export default FormItem(UploadButton);
