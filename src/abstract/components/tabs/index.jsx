import * as React from 'react';
import { Tabs, Tab } from '@material-ui/core';
export class Child extends React.Component {
}
export class Root extends React.Component {
    constructor(props) {
        super(props);
        this.renderData = () => {
            const data = React.Children.toArray(this.props.children);
            const props = data.map(m => m.props);
            const tabs = props.map((m, i) => <Tab label={m.label} value={i} key={i}/>);
            const containers = props.map((m, i) => m.children);
            const values = props.map(m => m.value);
            const indexOf = values.indexOf(this.props.value);
            const currentIndex = indexOf <= 0 ? 0 : indexOf >= values.length ? (values.length - 1 <= 0 ? 0 : values.length - 1) : indexOf;
            const currentContainer = containers[currentIndex];
            return { tabs, containers, values, currentIndex, currentContainer };
        };
        this.onChange = (index) => {
            const data = React.Children.toArray(this.props.children);
            const item = data[index];
            const props = item && item.props;
            const value = props && props.value;
            if (this.props.onChange)
                this.props.onChange(value);
        };
        this.state = {};
    }
    render() {
        const data = this.renderData();
        return (<div style={{ flexGrow: 1, backgroundColor: 'white' }}>
        <Tabs value={data.currentIndex} onChange={(e, value) => this.onChange(value)}>
          {data.tabs}
        </Tabs>
        {data.currentContainer}
      </div>);
    }
}
