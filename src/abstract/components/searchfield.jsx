import * as React from 'react';
import { AbstractComponent, FormItem } from './abstract';
import TextField from './textfield';
class Seachfield extends AbstractComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return <TextField {...this.props} fiDisable={true}/>;
    }
}
export default FormItem(Seachfield, { fiTitleType: 'InputLabel' });
