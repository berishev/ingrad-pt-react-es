import * as React from 'react';
import Divider from './divider';
function getObjectInOOFTGO(obj) {
    if (obj instanceof Function)
        return obj();
    return obj;
}
export default class ContentForm extends React.Component {
    constructor(props) {
        super(props);
        this.renderTitle = (title, left, right) => {
            return (<div style={{
                display: 'inline-flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                padding: '10px 0'
            }}>
        <div style={{
                display: 'inline-flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                width: '15%'
            }}>
          {left}
        </div>
        <div style={{
                display: 'inline-flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                width: '70%'
            }}>
          {title}
        </div>
        <div style={{
                display: 'inline-flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                width: '15%'
            }}>
          {right}
        </div>
      </div>);
        };
        this.renderSubitle = () => {
            let { title, disableTitle, extraLeftTitle, extraRightTitle } = this.props;
            title = disableTitle ? null : getObjectInOOFTGO(title);
            if (title == null)
                return null;
            return this.renderTitle(typeof title == 'string' ? <h3>{title}</h3> : title, extraLeftTitle, extraRightTitle);
        };
        this.renderHeaderBar = () => {
            let { headerBar } = this.props;
            return headerBar ? <div style={{ width: '100%', display: 'inline' }}>{getObjectInOOFTGO(headerBar)}</div> : null;
        };
        this.renderFooterBar = () => {
            let { footerBar } = this.props;
            return footerBar ? <div style={{ width: '100%', display: 'inline' }}>{getObjectInOOFTGO(footerBar)}</div> : null;
        };
    }
    render() {
        let headerBar = this.props.disableTitle ? null : this.renderHeaderBar();
        let title = this.props.disableTitle ? null : this.renderSubitle();
        title = title && (<div style={{ width: '100%' }}>
        {title}
        {!headerBar ? <Divider isHrTag={true}/> : null}
      </div>);
        let footerBar = this.renderFooterBar();
        return (<div style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: '100%',
            paddingTop: '10px'
        }}>
        {title || headerBar ? (<div style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: '10px'
        }}>
            {title}
            {headerBar}
          </div>) : null}
        <div style={{ width: '100%' }}>
          <div style={{ overflowY: 'auto', overflowX: 'hidden', height: '100%' }}>
            <div style={{ width: '100%' }}>{this.props.children}</div>
          </div>
        </div>
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%'
        }}>
          {footerBar}
        </div>
      </div>);
    }
}
