import { createStyles } from '@material-ui/core';
export default (theme) => createStyles({
    root: {
        height: 250,
        flexGrow: 1
    },
    container: {
        position: 'relative'
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 3,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0
    },
    suggestion: {
        display: 'block'
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none'
    },
    divider: {
        height: theme.spacing.unit * 2
    }
});
