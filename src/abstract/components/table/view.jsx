import * as React from 'react';
import { Close, Visibility } from '@material-ui/icons';
import Portal from 'berish-react-portals';
import * as moment from 'moment';
import { Button, Row } from '../';
import { LINQ } from 'berish-linq/dist';
import TableModal from './modal';
import LINQTable from './linq';
import RcView from './raw';
export default class TableView extends React.PureComponent {
    constructor(props) {
        super(props);
        this.prerender = (value) => {
            if (value == null)
                return '-';
            let type = typeof value;
            if (type === 'string')
                return value;
            else if (type === 'number')
                return this.prerender(`${value}`);
            else if (type === 'boolean')
                return this.prerender(value == true ? 'Да' : 'Нет');
            else if (value instanceof Date)
                return this.prerender(moment(value)
                    .locale('ru')
                    .format('MMMM Do YYYY, h:mm:ss'));
            else if (Array.isArray(value)) {
                let values = LINQ.fromArray(value).select(m => this.prerender(m));
                let isString = values.all(m => typeof m == 'string');
                return isString
                    ? values
                        .select(m => m)
                        .toArray()
                        .join('\n')
                    : values.select(m => m).toArray();
            }
            else
                return <span onClick={e => e.stopPropagation()}>{value}</span>;
        };
        this.keyFunction = (m) => (typeof m == 'object' && 'id' in m ? m['id'] : m);
        this.getRowSelectionConfig = () => {
            const selection = this.selection;
            if (selection.disabled)
                return null;
            let selectedItems = LINQ.fromArray(selection.items || []);
            const rawSelection = {
                items: selectedItems.toArray(),
                onSelect: (record, selected) => {
                    if (selection.on) {
                        if (selected) {
                            selectedItems = selectedItems.concat(record);
                        }
                        else {
                            selectedItems = selectedItems.where(m => this.keyFunction(m) != this.keyFunction(record));
                        }
                        selectedItems = selectedItems.distinct(this.keyFunction);
                        return selection.on(selectedItems.toArray());
                    }
                },
                onSelectAll: (records, selected) => {
                    if (selection.on) {
                        if (selected) {
                            selectedItems = LINQ.fromArray([...records]);
                        }
                        else {
                            selectedItems = LINQ.fromArray([]);
                        }
                        selectedItems = selectedItems.distinct(this.keyFunction);
                        return selection.on(selectedItems.toArray());
                    }
                }
            };
            return rawSelection;
        };
        this.renderSelectedField = () => {
            return (<Row style={{ margin: '0 0', justifyContent: 'center', alignItems: 'middle' }}>
        <span>Выбрано {this.selection.items.length} элементов</span>
        <Button icon={<Visibility />} style={{ marginLeft: '10px' }} shape="circle" placeholder="Посмотреть выбранные элементы" onClick={async () => {
                try {
                    let TableClassModal = Portal.create(TableModal);
                    let res = (await TableClassModal({
                        tableClass: LINQTable,
                        tableProps: {
                            data: this.selection.items || [],
                            pagination: false,
                            columns: this.props.columns
                        }
                    }));
                    if (res && res.length > 0) {
                        let rows = LINQ.fromArray(this.selection.items || []);
                        rows = rows.except(res).distinct(m => (typeof m == 'object' && 'id' in m ? m['id'] : m));
                        this.selection.on(rows.toArray());
                    }
                }
                catch (err) {
                    console.log(err);
                }
            }}/>
        <Button icon={<Close />} style={{ marginLeft: '10px' }} shape="circle" placeholder="Очистить выбранные элементы" onClick={() => this.selection.on([])}/>
      </Row>);
        };
        this.state = {
            currentColumn: null
        };
    }
    get selection() {
        return this.props.selection || {};
    }
    render() {
        return (<RcView {...this.props} dataSource={this.props.dataSource} columns={this.props.columns} prerender={this.prerender} loading={!!this.props.loading} title={!this.props.selection || true || !(this.selection.items && this.selection.items.length > 0) ? null : this.renderSelectedField} selection={this.props.selection && this.getRowSelectionConfig()} keyFunction={this.keyFunction} listenDrawer={true} containerMinusHeight={this.props.containerMinusHeight}/>);
    }
}
