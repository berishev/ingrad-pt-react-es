import * as React from 'react';
import { Table, Column, Cell, ColumnGroup } from 'fixed-data-table-2';
import 'fixed-data-table-2/dist/fixed-data-table.min.css';
import * as Dimensions from 'react-dimensions';
import { Checkbox } from '..';
import { storageController } from '../../global';
import Spin from '../spin';
class Raw extends React.Component {
    constructor(props) {
        super(props);
        this.unlistener = this.props.listenDrawer &&
            storageController.localStore.subscribe(m => {
                let drawerCollapsed = m.collapsed;
                if (this.state.drawerCollapsed != drawerCollapsed)
                    this.setState({ drawerCollapsed });
            });
        this.prerender = (value) => {
            const { prerender } = this.props;
            if (prerender)
                return prerender(value);
            return value;
        };
        this.getStyleColumn = (column, index) => {
            const { columnStyle: def } = this.props;
            const { columnStyle: custom } = column;
            const defaultStyle = typeof def == 'function' ? def(column, index) : def;
            const customStyle = typeof custom == 'function' ? custom(column, index) : custom;
            return Object.assign({}, defaultStyle || {}, customStyle || {});
        };
        this.getStyleCell = (column, index) => {
            const { rowStyle: def } = this.props;
            const { rowStyle: custom } = column;
            const defaultStyle = typeof def == 'function' ? def(this.props.dataSource[index], index) : def;
            const customStyle = typeof custom == 'function'
                ? custom(this.props.dataSource[index], index)
                : custom;
            return Object.assign({}, defaultStyle || {}, customStyle || {});
        };
        this.renderColumn = (column, index) => {
            const header = typeof column.title == 'function'
                ? this.prerender(column.title(this.props.dataSource))
                : this.prerender(column.title);
            const columnStyle = this.getStyleColumn(column, index);
            if (column.children && column.children.length > 0) {
                return (<ColumnGroup header={<Cell style={columnStyle}>{header}</Cell>} key={index} children={column.children.map((m, i) => this.renderColumn(m, index * 10 + i))}/>);
            }
            else {
                return (<Column header={<Cell style={columnStyle}>{header}</Cell>} cell={props => {
                    const rowStyle = this.getStyleCell(column, props.rowIndex);
                    let result = null;
                    if (column.render)
                        result = column.render(this.props.dataSource[props.rowIndex], props.rowIndex);
                    return (<Cell style={rowStyle} {...props}>
                {this.prerender(result)}
              </Cell>);
                }} width={column.width || 200} fixed={column.fixed} key={index} flexGrow={typeof column.flexGrow == 'undefined' ? 1 : column.flexGrow} minWidth={column.minWidth || 65} maxWidth={column.maxWidth}/>);
            }
        };
        this.renderAllColumn = () => {
            const { columns, selection, keyFunction } = this.props;
            let beforeColumns = [];
            if (selection) {
                const checkboxColumn = {
                    title: (<Checkbox fiDisable={true} disabledTooltip={true} value={selection.items.length == this.props.dataSource.length &&
                        selection.items.length > 0} disabled={this.props.dataSource.length <= 0} onChange={value => {
                        if (selection.onSelectAll)
                            selection.onSelectAll(value ? this.props.dataSource : [], value);
                    }}/>),
                    render: item => {
                        const index = selection.items
                            .map(keyFunction)
                            .indexOf(keyFunction(item));
                        return (<Checkbox fiDisable={true} disabledTooltip={true} value={index != -1} onChange={value => {
                            if (selection.onSelect)
                                selection.onSelect(item, value);
                        }}/>);
                    },
                    width: 65,
                    maxWidth: 65,
                    flexGrow: 0,
                    fixed: true
                };
                beforeColumns = [checkboxColumn];
            }
            return [...beforeColumns, ...columns].map((m, i) => this.renderColumn(m, i));
        };
        this.state = {
            checkAll: false,
            drawerCollapsed: storageController.localStore.collapsed
        };
    }
    componentWillUnmount() {
        if (this.unlistener)
            this.unlistener();
    }
    render() {
        const { containerHeight, containerWidth, containerMinusHeight } = this.props;
        const { drawerCollapsed } = this.state;
        // const width = drawerCollapsed
        //   ? containerWidth - drawerWidth - 80
        //   : containerWidth - drawerClosedWidth - 90;
        // const height = containerHeight - (containerMinusHeight || 220);
        const width = containerWidth;
        const height = containerHeight * 0.6;
        return (<Spin loading={this.props.loading}>
        <Table rowsCount={this.props.dataSource.length} rowHeight={75} headerHeight={50} width={width} height={height < 500 ? 500 : height}>
          {this.renderAllColumn()}
        </Table>
      </Spin>);
    }
}
export default Dimensions({
    getHeight: function (element) {
        return window.innerHeight;
    },
    getWidth: function (element) {
        return window.innerWidth;
    }
})(Raw);
