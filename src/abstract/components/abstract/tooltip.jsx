import * as React from 'react';
import { Tooltip as ATooltip } from '@material-ui/core';
export function Tooltip(element, config) {
    config = config || {};
    let disableTooltip = config.disableTooltip || false;
    let trigger = config.trigger || 'hover';
    let isButton = config.isButton || false;
    let defaultPlaceholder = isButton ? 'Нажмите, чтобы совершить действие' : 'Введите значение';
    let title = config.placeholder || defaultPlaceholder;
    if (disableTooltip)
        return element;
    return (<ATooltip title={title} disableHoverListener={trigger != 'hover'} disableFocusListener={trigger != 'focus'} disableTouchListener={trigger != 'click'}>
      {element}
    </ATooltip>);
}
