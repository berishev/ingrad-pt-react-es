import * as React from 'react';
import { PageController } from '../global/pageController';
import { Modal } from '../components';
import { storageController } from '../global';
export default class ModalRoute extends React.PureComponent {
    constructor(props) {
        super(props);
        this.systemUnlistener = storageController.systemStore.subscribe(m => this.forceUpdate());
        this.localStoreUnlistener = storageController.localStore.subscribe(m => this.forceUpdate());
        this.globalStoreUnlistener = storageController.globalStore.subscribe(m => this.forceUpdate());
        this.resolve = (resolveGeneric) => {
            this.props.resolve(resolveGeneric);
        };
        this.reject = () => {
            this.props.resolve();
        };
        this.updateParams = (params) => {
            let { controller } = this.state;
            controller = controller.receive(params);
            this.forceUpdate();
        };
        this.state = {
            controller: PageController.init(this.props.params)
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            controller: PageController.init(nextProps.params, this.state.controller)
        });
    }
    componentWillUnmount() {
        if (this.systemUnlistener)
            this.systemUnlistener();
        if (this.localStoreUnlistener)
            this.localStoreUnlistener();
        if (this.globalStoreUnlistener)
            this.globalStoreUnlistener();
    }
    render() {
        const controller = this.state.controller;
        const ComponentClass = this.props.component;
        return (<Modal.Form resolve={this.resolve} reject={this.reject} width="80vw">
        <ComponentClass controller={controller} modal={{
            resolve: this.resolve,
            reject: this.reject,
            getContainer: this.props.getContainer,
            updateParams: this.updateParams
        }}/>
      </Modal.Form>);
    }
}
